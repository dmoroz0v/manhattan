#import "MHNavigationRouter.h"
#import "MHNavigationRouterVC.h"
#import "MHListPresenter.h"
#import "MHImagesLoaderService.h"
#import "MHContainer.h"
#import "MHListVC.h"

@interface MHNavigationRouter()

@property (nonatomic, strong) MHNavigationRouterVC *navigationRouterVC;
@property (nonatomic, strong) MHContainer *container;

@property (nonatomic, strong) MHListPresenter *listPresenter;

@end

@implementation MHNavigationRouter

- (instancetype)initWithNavigationRouterVC:(MHNavigationRouterVC *)navigationRouterVC
                                 container:(MHContainer *)container
{
	self = [super init];

	_navigationRouterVC = navigationRouterVC;
	_container = container;

	UIViewController<MHListUI> *listUI = [[MHListVC alloc] init];
	_listPresenter = [container makeListPresenterWithUI:listUI];

	_navigationRouterVC.viewControllers = @[ listUI ];

	return self;
}

@end
