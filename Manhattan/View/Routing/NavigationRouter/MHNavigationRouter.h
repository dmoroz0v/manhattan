@class MHNavigationRouterVC;
@class MHContainer;

@interface MHNavigationRouter : NSObject

- (instancetype)initWithNavigationRouterVC:(MHNavigationRouterVC *)navigationRouterVC
                                 container:(MHContainer *)container;

@end
