#import "MHListItemUI.h"

@interface MHListItemViewCell : UICollectionViewCell <MHListItemUI>

- (void)willReuse;
- (void)setHorizontalPadding:(CGFloat)padding;
- (void)moveContentToRightWithCompletion:(void(^)(BOOL finished))completion;

@end
