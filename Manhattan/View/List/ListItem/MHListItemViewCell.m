#import "MHListItemViewCell.h"

@interface MHListItemViewCell ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation MHListItemViewCell

@synthesize delegate = _delegate;

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	self.backgroundColor = UIColor.clearColor;

	_imageView = [[UIImageView alloc] init];
	_imageView.backgroundColor = UIColor .lightGrayColor;
	_imageView.contentMode = UIViewContentModeScaleAspectFill;
	_imageView.frame = self.contentView.bounds;
	_imageView.clipsToBounds = YES;
	_imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

	[self.contentView addSubview:_imageView];

	return self;
}

- (UIImage *)image
{
	return self.imageView.image;
}

- (void)setImage:(UIImage *)image
{
	[self.imageView replaceImage:image duration:0.25];
}

- (void)willReuse
{
	self.imageView.frame = self.contentView.bounds;

	[self.delegate willReuseUI];
}

- (void)setHorizontalPadding:(CGFloat)padding
{
	self.imageView.frame = CGRectInset(self.imageView.frame, padding, 0);
}

- (void)moveContentToRightWithCompletion:(void(^)(BOOL finished))completion
{
	void (^ const animations)() = ^{
		CGRect frame = self.imageView.frame;
		frame.origin.x = self.frame.size.width;
		self.imageView.frame = frame;
	};

	[UIView animateWithDuration:0.25 animations:animations completion:completion];
}

@end
