#import "MHListVC.h"
#import "MHListItemViewCell.h"

static CGFloat const kSpace = 10;
static NSString * const kCellIdentifier = @"kCellIdentifier";

@interface MHListVC ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) UICollectionView *listView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation MHListVC

@synthesize delegate = _delegate;

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.view.backgroundColor = UIColor.whiteColor;

	self.layout = [[UICollectionViewFlowLayout alloc] init];
	self.layout.minimumLineSpacing = kSpace;
	self.layout.sectionInset = UIEdgeInsetsMake(kSpace, 0, kSpace, 0);

	self.listView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:self.layout];
	self.listView.backgroundColor = UIColor.whiteColor;
	self.listView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.listView.delegate = self;
	self.listView.dataSource = self;

	[self.listView registerClass:[MHListItemViewCell class] forCellWithReuseIdentifier:kCellIdentifier];

	[self.view addSubview:self.listView];

	self.refreshControl = [[UIRefreshControl alloc] init];
	[self.refreshControl addTarget:self action:@selector(startRefresh) forControlEvents:UIControlEventValueChanged];
	self.listView.alwaysBounceVertical = YES;
	[self.listView addSubview:self.refreshControl];
}

- (void)viewWillLayoutSubviews
{
	[super viewWillLayoutSubviews];

	const CGFloat height = self.listView.bounds.size.width - kSpace * 2;
	self.layout.itemSize = CGSizeMake(self.listView.bounds.size.width, height);

	[self.layout invalidateLayout];

	[self.listView sendSubviewToBack:self.refreshControl];
}

- (void)startRefresh
{
	[self.delegate reloadItemsWithCompletion:^{
		[self.refreshControl endRefreshing];
		[self.listView reloadData];
	}];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.delegate numberOfItems];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	MHListItemViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier
	                                                                     forIndexPath:indexPath];

	[cell willReuse];
	[cell setHorizontalPadding:kSpace];

	[self.delegate willShowListItemUI:cell forIndex:indexPath.row];

	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	self.listView.userInteractionEnabled = NO;

	MHListItemViewCell *cell = (MHListItemViewCell *)[self.listView cellForItemAtIndexPath:indexPath];

	[cell moveContentToRightWithCompletion:^(BOOL _) {

		void (^ const updates)() = ^{
			[self.listView deleteItemsAtIndexPaths:@[indexPath]];
			[self.delegate didDeleteItemAtIndex:indexPath.row];
		};

		[self.listView performBatchUpdates:updates completion:^(BOOL __) {

			self.listView.userInteractionEnabled = YES;
		}];

	}];
}

@end
