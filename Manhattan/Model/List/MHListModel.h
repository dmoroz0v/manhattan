@class MHListItemModel;

@interface MHListModel : NSObject

@property (strong, readonly) NSArray<MHListItemModel *> *items;

@end
