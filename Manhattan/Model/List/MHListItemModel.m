#import "MHListItemModel.h"
#import "MHListUIDelegate.h"

@implementation MHListItemModel

- (instancetype)initWithImageUrl:(NSURL *)imageUrl
{
	self = [super init];

	_imageUrl = imageUrl;

	return self;
}

@end
