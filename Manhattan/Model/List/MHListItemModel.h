@interface MHListItemModel : NSObject

@property (nonatomic, readonly) NSURL *imageUrl;

- (instancetype)initWithImageUrl:(NSURL *)imageUrl;

@end
