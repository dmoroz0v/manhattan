#import "MHListModel.h"
#import "MHListItemModel.h"

@implementation MHListModel

- (instancetype)init
{
	self = [super init];

	_items = @[
		[[MHListItemModel alloc] initWithImageUrl:[NSURL URLWithString:@"http://s8.pikabu.ru/post_img/2016/05/25/4/og_og_1464149297225295713.jpg"]],
		[[MHListItemModel alloc] initWithImageUrl:[NSURL URLWithString:@"https://i.ytimg.com/vi/XOH4RLBZP3Y/maxresdefault.jpg"]],
		[[MHListItemModel alloc] initWithImageUrl:[NSURL URLWithString:@"http://nahnews.org/wp-content/uploads/2016/08/399.jpg"]],
		[[MHListItemModel alloc] initWithImageUrl:[NSURL URLWithString:@"https://cdn0.vox-cdn.com/thumbor/S0lB5HvM-yCFpjLyPrQPZNOYNws=/0x67:1280x787/1600x900/cdn0.vox-cdn.com/uploads/chorus_image/image/52254071/baby_groot.0.jpeg"]],
		[[MHListItemModel alloc] initWithImageUrl:[NSURL URLWithString:@"http://www.moviequotesandmore.com/wp-content/uploads/social-network-20.jpg"]],
		[[MHListItemModel alloc] initWithImageUrl:[NSURL URLWithString:@"https://s-media-cache-ak0.pinimg.com/originals/79/fc/52/79fc525029e13f75ee66284afa77214a.png"]],
	];

	return self;
}

@end
