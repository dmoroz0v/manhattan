@class MHListModel;

@interface MHDataManager : NSObject

- (MHListModel *)fetchListModel;

@end
