#import "NSURL+SHA256.h"

#import <CommonCrypto/CommonDigest.h>

@implementation NSURL (SHA256)

- (NSString *)sha256
{
	const char* str = [self.path UTF8String];
	unsigned char result[CC_SHA256_DIGEST_LENGTH];
	CC_SHA256(str, (CC_LONG)strlen(str), result);

	NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
	for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; ++i)
	{
		[ret appendFormat:@"%02x",result[i]];
	}
	return ret;
}

@end
