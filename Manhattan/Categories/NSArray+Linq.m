#import "NSArray+Linq.h"

@implementation NSArray (Linq)

- (NSArray *)map:(id (^)(id item))map
{
	if (map == nil) return [self copy];

	NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:self.count];

	for (id item in self)
	{
		[result addObject:map(item)];
	}

	return [result copy];
}

@end
