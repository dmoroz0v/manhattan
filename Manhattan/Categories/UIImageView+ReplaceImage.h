@interface UIImageView (ReplaceImage)

- (void)replaceImage:(UIImage *)image duration:(CGFloat)duration;

@end
