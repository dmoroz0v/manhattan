@interface NSURL (SHA256)

@property (nonatomic, strong, readonly) NSString *sha256;

@end
