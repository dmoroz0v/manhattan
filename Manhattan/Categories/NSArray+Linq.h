@interface NSArray<ObjectType> (Linq)

- (NSArray *)map:(id (^)(ObjectType item))map;

@end
