#import "UIImageView+ReplaceImage.h"

@implementation UIImageView (ReplaceImage)

- (void)replaceImage:(UIImage *)image duration:(CGFloat)duration
{
	self.image = image;

	CATransition *transition = [CATransition animation];
	transition.duration = duration;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionFade;

	[self.layer addAnimation:transition forKey:nil];
}

@end
