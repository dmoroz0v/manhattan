#import "MHAppDelegate.h"
#import "MHNavigationRouterVC.h"
#import "MHNavigationRouter.h"
#import "MHDataManager.h"
#import "MHImagesLoaderService.h"
#import "MHFilesStorageService.h"
#import "MHImagesCacheService.h"
#import "MHContainer.h"

@interface MHAppDelegate ()

@property (nonatomic, strong) MHNavigationRouter *navigationVCRouter;

@end

@implementation MHAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	NSURL *downloadedImages =
		[NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"downloadedImages"]];

	MHNavigationRouterVC *navigationRouterVC = [[MHNavigationRouterVC alloc] init];
	MHDataManager *dataManager = [[MHDataManager alloc] init];
	MHFilesStorageService *imagesStorageService = [[MHFilesStorageService alloc] initWithRelativeUrl:downloadedImages];
	MHImagesCacheService *imagesCacheService = [[MHImagesCacheService alloc] initWithCacheStorage:imagesStorageService];
	MHImagesLoaderService *imageLoaderService = [[MHImagesLoaderService alloc] initWithImagesCache:imagesCacheService];

	MHContainer *container = [[MHContainer alloc] initWithDataManager:dataManager
	                                               imageLoaderService:imageLoaderService];

	self.navigationVCRouter = [[MHNavigationRouter alloc] initWithNavigationRouterVC:navigationRouterVC
	                                                                       container:container];

	self.window = [[UIWindow alloc] init];
	self.window.rootViewController = navigationRouterVC;
	[self.window makeKeyAndVisible];

	return YES;
}

@end
