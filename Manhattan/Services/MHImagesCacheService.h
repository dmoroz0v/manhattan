@class MHFilesStorageService;

@interface MHImagesCacheService : NSObject

- (instancetype)initWithCacheStorage:(MHFilesStorageService *)cacheStorage;

- (void)saveImage:(UIImage *)image forName:(NSString *)name;

- (UIImage *)imageForName:(NSString *)name;

@end
