@class MHImagesCacheService;

@interface MHImagesLoaderService : NSObject

- (instancetype)initWithImagesCache:(MHImagesCacheService *)imagesCache;

- (void)loadImageFromURL:(NSURL *)url callback:(void(^)(UIImage *image))callback;

@end
