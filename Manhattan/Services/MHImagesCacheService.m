#import "MHImagesCacheService.h"
#import "MHFilesStorageService.h"

@interface MHImagesCacheService ()

@property (nonatomic, strong) MHFilesStorageService *cacheStorage;
@property (nonatomic, strong) NSMutableDictionary<NSString *, UIImage *> *memoryCache;

@end

@implementation MHImagesCacheService

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
	                                                name:UIApplicationDidReceiveMemoryWarningNotification
	                                              object:nil];
}

- (instancetype)initWithCacheStorage:(MHFilesStorageService *)cacheStorage
{
	self = [super init];

	_cacheStorage = cacheStorage;
	_memoryCache = [[NSMutableDictionary alloc] init];

	[[NSNotificationCenter defaultCenter] addObserver:self
	                                         selector:@selector(didReceiveMemoryWarning)
	                                             name:UIApplicationDidReceiveMemoryWarningNotification
	                                           object:nil];

	return self;
}

- (void)didReceiveMemoryWarning
{
	@synchronized (self.memoryCache)
	{
		[self.memoryCache removeAllObjects];
	}
}

- (void)saveImage:(UIImage *)image forName:(NSString *)name
{
	if (image != nil)
	{
		@synchronized (self.memoryCache)
		{
			self.memoryCache[name] = image;
		}
	}

	[self.cacheStorage saveData:UIImagePNGRepresentation(image)
	               withFilename:name];
}

- (UIImage *)imageForName:(NSString *)name
{
	NSData *data = [self.cacheStorage dataForFilename:name];

	if (data != nil)
	{
		UIImage *image = [UIImage imageWithData:data];

		if (image != nil)
		{
			@synchronized (self.memoryCache)
			{
				self.memoryCache[name] = image;
			}
		}

		return image;
	}

	return nil;
}

@end
