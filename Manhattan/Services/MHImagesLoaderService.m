#import "MHImagesLoaderService.h"
#import "MHImagesCacheService.h"

@interface MHImagesLoaderService ()

@property (nonatomic, strong) MHImagesCacheService *imagesCache;
@property (nonatomic, strong) NSURLSession *session;

@end

@implementation MHImagesLoaderService

- (instancetype)initWithImagesCache:(MHImagesCacheService *)imagesCache
{
	self = [super init];

	_imagesCache = imagesCache;

	NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];

	_session = [NSURLSession sessionWithConfiguration:sessionConfig];

	return self;
}

- (void)loadImageFromURL:(NSURL *)url callback:(void(^)(UIImage *image))callback
{
	void (^callbackCopied)(UIImage *) = [callback copy];

	UIImage *cachedImage = nil;

	if (url != nil)
	{
		cachedImage = [self.imagesCache imageForName:url.sha256];
	}

	if (cachedImage != nil || url == nil)
	{
		if (callbackCopied != nil)
		{
			dispatch_async(dispatch_get_main_queue(), ^{
				callbackCopied(cachedImage);
			});
		}
		return;
	}

	__weak MHImagesLoaderService *weakSelf = self;

	[[self.session
		downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {

			MHImagesLoaderService *self = weakSelf;

			if (error != nil)
			{
				NSLog(@"ImageLoaderService error '%@'", error);
			}

			UIImage *image = nil;

			if (location != nil)
			{
				image = [UIImage imageWithContentsOfFile:location.path];

				if (image != nil) {

					[self.imagesCache saveImage:image forName:url.sha256];
				}
			}

			if (callbackCopied != nil)
			{
				dispatch_async(dispatch_get_main_queue(), ^{
					callbackCopied(image);
				});
			}
		}]
		resume];
}

@end
