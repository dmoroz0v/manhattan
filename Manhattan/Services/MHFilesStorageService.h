@interface MHFilesStorageService : NSObject

- (instancetype)initWithRelativeUrl:(NSURL *)relativeUrl;

- (NSData *)dataForFilename:(NSString *)filename;

- (void)saveData:(NSData *)data withFilename:(NSString *)filename;

@end
