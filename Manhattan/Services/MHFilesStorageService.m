#import "MHFilesStorageService.h"

@interface MHFilesStorageService ()

@property (nonatomic, copy) NSURL *relativeUrl;

@end

@implementation MHFilesStorageService

- (instancetype)initWithRelativeUrl:(NSURL *)relativeUrl
{
	self = [super init];

	_relativeUrl = relativeUrl;

	NSFileManager *fileManager = [NSFileManager defaultManager];

	if (![fileManager fileExistsAtPath:_relativeUrl.path])
	{
		NSError *error = nil;
		BOOL success = [fileManager createDirectoryAtURL:_relativeUrl
		                     withIntermediateDirectories:YES
		                                      attributes:nil
		                                           error:&error];

		if (!success)
		{
			NSLog(@"fileManager createDirectoryAtURL error '%@'", error);
		}
	}

	return self;
}

- (NSData *)dataForFilename:(NSString *)filename
{
	return [NSData dataWithContentsOfURL:[self.relativeUrl URLByAppendingPathComponent:filename]];
}

- (void)saveData:(NSData *)data withFilename:(NSString *)filename
{
	if (data == nil || filename == nil) return;

	NSFileManager *fileManager = [NSFileManager defaultManager];

	NSURL *toUrl = [self.relativeUrl URLByAppendingPathComponent:filename];

	if ([fileManager fileExistsAtPath:toUrl.path])
	{
		NSError *error = nil;
		BOOL success = [fileManager removeItemAtURL:toUrl error:&error];

		if (!success)
		{
			NSLog(@"fileManager removeItemAtURL error '%@'", error);
		}
	}

	NSError *error = nil;
	BOOL success = [data writeToURL:toUrl options:NSAtomicWrite error:&error];

	if (!success)
	{
		NSLog(@"fileManager copyItemAtURL error '%@'", error);
	}
}

@end
