@class MHImagesLoaderService;
@class MHDataManager;
@protocol MHListUI;
@class MHListPresenter;

@interface MHContainer : NSObject

- (instancetype)initWithDataManager:(MHDataManager *)dataManager
                 imageLoaderService:(MHImagesLoaderService *)imageLoaderService;

- (MHListPresenter *)makeListPresenterWithUI:(id<MHListUI>)listUI;

@end
