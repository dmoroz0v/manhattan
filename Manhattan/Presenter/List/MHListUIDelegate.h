@protocol MHListItemUI;

@protocol MHListUIDelegate<NSObject>

- (NSInteger)numberOfItems;

- (void)willShowListItemUI:(id<MHListItemUI>)listItemUI forIndex:(NSInteger)index;

- (void)didDeleteItemAtIndex:(NSInteger)index;

- (void)reloadItemsWithCompletion:(void (^)())callback;

@end
