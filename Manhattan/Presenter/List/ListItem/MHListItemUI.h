@protocol MHListItemViewCellDelegate <NSObject>

- (void)willReuseUI;

@end

@protocol MHListItemUI<NSObject>

@property (nonatomic, weak) id<MHListItemViewCellDelegate> delegate;

@property (nonatomic, strong) UIImage *image;

@end
