@protocol MHListItemUI;
@class MHListItemModel;
@class MHImagesLoaderService;

@interface MHListItemPresenter : NSObject

@property (nonatomic, strong) id<MHListItemUI> listItemUI;

- (instancetype)initListItemModel:(MHListItemModel *)listItemModel
               imageLoaderService:(MHImagesLoaderService *)imageLoaderService;

@end
