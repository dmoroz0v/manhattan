#import "MHListItemPresenter.h"
#import "MHListItemUI.h"
#import "MHListItemModel.h"
#import "MHImagesLoaderService.h"
#import "MHListItemViewCell.h"

@interface MHListItemPresenter () <MHListItemViewCellDelegate>

@property (nonatomic, strong) MHListItemModel *listItemModel;

@property (nonatomic, strong) MHImagesLoaderService *imageLoaderService;

@property (nonatomic, assign) BOOL imageLoading;

@end

@implementation MHListItemPresenter

- (instancetype)initListItemModel:(MHListItemModel *)listItemModel
               imageLoaderService:(MHImagesLoaderService *)imageLoaderService
{
	self = [super init];
	
	_listItemModel = listItemModel;
	_imageLoaderService = imageLoaderService;

	return self;
}

- (void)setListItemUI:(id<MHListItemUI>)listItemUI
{
	self.listItemUI.delegate = nil;

	_listItemUI = listItemUI;

	self.listItemUI.delegate = self;
	self.listItemUI.image = nil;

	if (self.listItemModel != nil && !self.imageLoading)
	{
		self.imageLoading = YES;

		__weak MHListItemPresenter *weakSelf = self;

		[self.imageLoaderService loadImageFromURL:self.listItemModel.imageUrl callback:^(UIImage *image) {
			MHListItemPresenter *self = weakSelf;
			self.imageLoading = NO;
			self.listItemUI.image = image;
		}];
	}
}

// MARK: MHListItemViewCellDelegate

- (void)willReuseUI
{
	self.listItemUI = nil;
}

@end
