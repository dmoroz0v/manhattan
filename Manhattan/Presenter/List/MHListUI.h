#import "MHListUIDelegate.h"

@protocol MHListUI<NSObject>

@property (nonatomic, weak) id<MHListUIDelegate> delegate;

@end
