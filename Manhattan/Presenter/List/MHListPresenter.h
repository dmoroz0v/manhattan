@protocol MHListUI;
@class MHListModel;
@class MHImagesLoaderService;

@interface MHListPresenter : NSObject

- (instancetype)initWithListVC:(id<MHListUI>)listVC
                         model:(MHListModel *)model
            imageLoaderService:(MHImagesLoaderService *)imageLoaderService;

@end
