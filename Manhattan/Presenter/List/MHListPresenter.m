#import "MHListPresenter.h"

#import "MHListModel.h"
#import "MHListUI.h"
#import "MHListItemPresenter.h"
#import "MHImagesLoaderService.h"

@interface MHListPresenter () <MHListUIDelegate>

@property (nonatomic, strong) id<MHListUI> listUI;
@property (nonatomic, strong) MHListModel *model;

@property (nonatomic, strong) NSMutableArray<MHListItemPresenter *> *listItemPresenters;

@property (nonatomic, strong) MHImagesLoaderService *imageLoaderService;
@end

@implementation MHListPresenter

- (instancetype)initWithListVC:(id<MHListUI>)listUI
                         model:(MHListModel *)model
            imageLoaderService:(MHImagesLoaderService *)imageLoaderService
{
	self = [super init];

	_listUI = listUI;
	_listUI.delegate = self;
	_model = model;
	_imageLoaderService = imageLoaderService;

	_listItemPresenters = [self presentersFromListItems:_model.items
	                                 imageLoaderService:_imageLoaderService];

	return self;
}

- (NSInteger)numberOfItems
{
	return self.listItemPresenters.count;
}

- (void)willShowListItemUI:(id<MHListItemUI>)listItemUI forIndex:(NSInteger)index
{
	self.listItemPresenters[index].listItemUI = listItemUI;
}

- (void)didDeleteItemAtIndex:(NSInteger)index
{
	[self.listItemPresenters removeObjectAtIndex:index];
}

- (void)reloadItemsWithCompletion:(void (^)())callback
{
	self.listItemPresenters = [self presentersFromListItems:self.model.items
	                                     imageLoaderService:self.imageLoaderService];

	if (callback != nil)
	{
		callback();
	}
}

// MARK: Private

- (NSMutableArray<MHListItemPresenter *> *)presentersFromListItems:(NSArray<MHListItemModel *> *)listItems 
                                                imageLoaderService:(MHImagesLoaderService *)imageLoaderService
{
	return [[self.model.items
		map:^MHListItemPresenter *(MHListItemModel *listItemModel) {
			return [[MHListItemPresenter alloc] initListItemModel:listItemModel
			                                   imageLoaderService:imageLoaderService];
		}]
		mutableCopy];
}

@end
