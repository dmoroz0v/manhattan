#import "MHContainer.h"
#import "MHImagesLoaderService.h"
#import "MHDataManager.h"
#import "MHListUI.h"
#import "MHListPresenter.h"

@interface MHContainer ()

@property (nonatomic, strong) MHDataManager *dataManager;
@property (nonatomic, strong) MHImagesLoaderService *imageLoaderService;

@end

@implementation MHContainer

- (instancetype)initWithDataManager:(MHDataManager *)dataManager
                 imageLoaderService:(MHImagesLoaderService *)imageLoaderService
{
	self = [super init];
	
	_dataManager = dataManager;
	_imageLoaderService = imageLoaderService;

	return self;
}

- (MHListPresenter *)makeListPresenterWithUI:(id<MHListUI>)listUI
{
	return [[MHListPresenter alloc] initWithListVC:listUI
	                                         model:self.dataManager.fetchListModel
	                            imageLoaderService:self.imageLoaderService];
}

@end
